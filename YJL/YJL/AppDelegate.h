//
//  AppDelegate.h
//  YJL
//
//  Created by tlian on 15/4/17.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

