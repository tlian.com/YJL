//
//  YJLHomeViewController.m
//  YJL
//
//  Created by tlian on 15/4/20.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import "YJLHomeViewController.h"
#import "UIBarButtonItem+YJL.h"

@interface YJLHomeViewController ()

@end

@implementation YJLHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // 设置导航栏内容
    [self setupNavBar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 *  设置导航栏内容
 */
-(void)setupNavBar{
    // 左边图标
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithIcon:@"logo" highIcon:@"logo" target:self action:nil];
    // 右边按钮
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithIcon:@"shopcart" highIcon:@"shopcart" target:self action:@selector(shopCartButtonClick:)];
    // 设置中间searchBar
    UIImage *searchImage = [UIImage imageNamed:@"searchbar_nomal"];
    UIImage *searchSelectedImage = [UIImage imageNamed:@"searchbar_select"];
    UIButton *titleButton = [[UIButton alloc] init];
    titleButton.frame = CGRectMake(0, 0, 172, 28);
    [titleButton setImage:searchImage forState:UIControlStateNormal];
    [titleButton setImage:searchSelectedImage forState:UIControlStateHighlighted];
    self.navigationItem.titleView = titleButton;
    
}

-(void)shopCartButtonClick:(UIBarButtonItem *)sender{
    NSLog(@"shopCartbuttonClick");
}
@end
