//
//  YJLTitleButton.h
//  YJL
//
//  Created by tlian on 15/4/20.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YJLTitleButton : UIButton
+ (instancetype) titleButton;
@end
