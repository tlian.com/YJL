//
//  YJLTabBarViewController.m
//  YJL
//
//  Created by tlian on 15/4/20.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import "YJLHomeViewController.h"
#import "YJLListViewController.h"
#import "YJLLogisticsViewController.h"
#import "YJLNavigationController.h"
#import "YJLPersonalViewController.h"
#import "YJLShoppingCartViewController.h"
#import "YJLTabBarViewController.h"
#import "YJLtabBar.h"

@interface YJLTabBarViewController ()<YJLTabBarDelegate>

@property (nonatomic, weak) YJLtabBar *customTabBar;

@end

@implementation YJLTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // 初始化tabbar
    [self setupTabbar];
    // 初始化所有子控制器
    [self setupAllChildViewControllers];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // 删除系统自动生成的UITabBarButton
    for (UIView *child in self.tabBar.subviews) {
        if ([child isKindOfClass:[UIControl class]]) {
            [child removeFromSuperview];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 *  初始化tabbar
 */
-(void)setupTabbar{
    if (self.customTabBar == nil) {
        YJLtabBar *customTabBar = [[YJLtabBar alloc] init];
        customTabBar.frame = self.tabBar.bounds;
        customTabBar.delegate = self;
        [self.tabBar addSubview:customTabBar];
        self.customTabBar = customTabBar;
    }
   
}

-(void)tabBar:(YJLtabBar *)tabBar didSelectedButtonFrom:(int)from to:(int)to{
    self.selectedIndex = to;
}

-(void)setupAllChildViewControllers{
    // 首页
    YJLHomeViewController *home = [[YJLHomeViewController alloc] init];
    home.view.backgroundColor = [UIColor whiteColor];
    [self setupChildViewController:home title:@"首页" imageName:@"tab_home_nomal" selectedImageName:@"tab_home_selected"];
    
    // 菜单
    YJLListViewController *list = [[YJLListViewController alloc] init];
    [self setupChildViewController:list title:@"菜单" imageName:@"tab_menu_nomal" selectedImageName:@"tab_menu_selected"];
    
    // 购物车
    YJLShoppingCartViewController * shoppingCart = [[YJLShoppingCartViewController alloc] init];
    [self setupChildViewController:shoppingCart title:@"购物车" imageName:@"tab_shopcart_nomal" selectedImageName:@"tab_shopcart_selected"];
    
    // 个人中心
    YJLPersonalViewController *personal = [[YJLPersonalViewController alloc] init];
    [self setupChildViewController:personal title:@"个人中心" imageName:@"tab_men_nomal" selectedImageName:@"tab_man_selected"];
    
    // 物流查询
    YJLLogisticsViewController *logistics = [[YJLLogisticsViewController alloc] init];
    [self setupChildViewController:logistics title:@"物流查询" imageName:@"tab_logis_nomal" selectedImageName:@"tab_logis_selected"];
}

- (void)setupChildViewController:(UIViewController *)childVc title:(NSString *)title imageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName
{
    // 1.设置控制器的属性
    childVc.title = title;
    // 设置图标
    childVc.tabBarItem.image = [UIImage imageNamed:imageName];
    // 设置选中的图标
    UIImage *selectedImage = [UIImage imageNamed:selectedImageName];
//    if (iOS7) {
        childVc.tabBarItem.selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    } else {
//        childVc.tabBarItem.selectedImage = selectedImage;
//    }
    
    // 2.包装一个导航控制器
    YJLNavigationController *nav = [[YJLNavigationController alloc] initWithRootViewController:childVc];
    [self addChildViewController:nav];
    
    // 3.添加tabbar内部的按钮
    [self.customTabBar addTabBarButtonWithItem:childVc.tabBarItem];
}

@end
