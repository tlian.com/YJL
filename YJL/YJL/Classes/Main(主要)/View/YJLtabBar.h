//
//  YJLtabBar.h
//  YJL
//
//  Created by tlian on 15/4/20.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YJLtabBar;
@protocol YJLTabBarDelegate <NSObject>

@optional

-(void)tabBar:(YJLtabBar *)tabbar didSelectedButtonFrom:(int)from to:(int)to;

@end

@interface YJLtabBar : UIView

@property (nonatomic, weak) id<YJLTabBarDelegate> delegate;

-(void)addTabBarButtonWithItem:(UITabBarItem *)item;


@end
