//
//  UIImage+YJL.m
//  YJL
//
//  Created by tlian on 15/4/20.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import "UIImage+YJL.h"

@implementation UIImage (YJL)

+ (UIImage *)imageWithName:(NSString *)name
{
        UIImage *image = [UIImage imageNamed:name];
        if (image == nil) { // 没有_os7后缀的图片
            image = [UIImage imageNamed:name];
        }
        return image;
}

+ (UIImage *)resizedImageWithName:(NSString *)name
{
    UIImage *image = [self imageWithName:name];
    return [image stretchableImageWithLeftCapWidth:image.size.width * 0.5 topCapHeight:image.size.height * 0.5];
}

@end
