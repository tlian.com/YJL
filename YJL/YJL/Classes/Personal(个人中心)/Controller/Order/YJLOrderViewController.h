//
//  YJLOrderViewController.h
//  YJL
//
//  Created by gtkg－mac on 15-4-27.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HYSegmentedControl.h"

@interface YJLOrderViewController : UIViewController<HYSegmentedControlDelegate,UITableViewDelegate,UITableViewDataSource>
/**
 *  分段控制器
 */
@property (strong, nonatomic) HYSegmentedControl *segmentedControl;

/**
 *  订单列表
 */
@property (strong, nonatomic) UITableView *orderTableView;

/**
 *  当前订单列表的数据
 */
@property (strong, nonatomic) NSMutableArray *orderArray;

@end
