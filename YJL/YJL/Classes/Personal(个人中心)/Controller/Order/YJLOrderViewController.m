//
//  YJLOrderViewController.m
//  YJL
//
//  Created by gtkg－mac on 15-4-27.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import "YJLOrderViewController.h"
#import "YJLOrderTableViewCell.h"

@interface YJLOrderViewController ()

@end

@implementation YJLOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    
//    原因是因为iOS7的View默认是渗透到状态栏上面的，解决的办法有很多，你可以设置成向上不渗透（也可以设置很多渗透的方向），或者你可以用storyboard里的布局方法来避免这种情况。  不加这句话，然后分段控制器里面的东西会下沉很多，这是在有导航栏情况下，不用导航栏进入的视图，不加上这句话都可以。
    self.automaticallyAdjustsScrollViewInsets =NO;
    
    //创建分段控制器
    [self createSegmentedControl];
    
    //添加tableView
    [self createTableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 *  创建分段控制器
 */
- (void)createSegmentedControl
{
    self.segmentedControl = [[HYSegmentedControl alloc] initWithOriginY:70 Titles:@[@"全部订单", @"待发货", @"待收获", @"待付款", @"待评价", ] delegate:self] ;
    [self.view addSubview:_segmentedControl];
}

//代理函数 获取当前下标
- (void)hySegmentedControlSelectAtIndex:(NSInteger)index{
    NSLog(@"%ld",(long)index);
}

/**
 *  创建tableView
 */
-(void)createTableView{
    self.orderTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 110, SCREEN_WIDTH, SCREEN_HEIGHT-110)];
    self.orderTableView.delegate = self;
    self.orderTableView.dataSource = self;
    self.orderTableView.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:self.orderTableView];
}


#pragma mark- TableView datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    return self.orderArray.count;
    return 4;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 170;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdenetify = @"orderCell";
    BOOL nib = NO;
    if (!nib) {
        [tableView registerNib:[UINib nibWithNibName:@"YJLOrderTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdenetify];
        nib = YES;
    }
    YJLOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdenetify];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.orderName.text = @"男士休闲衬衫春季衬衫值得入手韩版长衬衫";
    cell.orderState.text = @"发货中...";
    [cell.orderAction setTitle:@"订单事件" forState:UIControlStateNormal];
    return cell;

}

/**
 *  返回界面
 */
-(void)backVC{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
