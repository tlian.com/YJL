//
//  YJLFindPasswordViewController.h
//  YJL
//
//  Created by gtkg－mac on 15-4-22.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YJLFindPasswordViewController : UIViewController
//手机号/邮箱/用户名
@property (weak, nonatomic) IBOutlet UITextField *userName;
//请输入验证码
@property (weak, nonatomic) IBOutlet UITextField *code;
//下一步按钮
@property (weak, nonatomic) IBOutlet UIButton *nextButton;



@end
