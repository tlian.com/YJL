//
//  YJLFindPasswordViewController.m
//  YJL
//
//  Created by gtkg－mac on 15-4-22.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import "YJLFindPasswordViewController.h"
#import "UIBarButtonItem+YJL.h"
#import "YJLWriteCodeViewController.h"

@interface YJLFindPasswordViewController ()

@end

@implementation YJLFindPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.userName.layer.masksToBounds = YES;
    self.userName.layer.cornerRadius = 4;
    self.userName.layer.borderWidth = 1;
    self.userName.layer.borderColor = [UIColor grayColor].CGColor;
    
    self.code.layer.masksToBounds = YES;
    self.code.layer.cornerRadius = 4;
    self.code.layer.borderWidth = 1;
    self.code.layer.borderColor = [UIColor grayColor].CGColor;
    
    self.nextButton.layer.masksToBounds = YES;
    self.nextButton.layer.borderWidth = 1;
    self.nextButton.layer.borderColor = [UIColor grayColor].CGColor;
    self.nextButton.layer.cornerRadius = 4;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// 验证码刷新
- (IBAction)codeAction:(id)sender {
}

//下一步
- (IBAction)nextAction:(id)sender {
    YJLWriteCodeViewController *writeCodeVC = [[YJLWriteCodeViewController alloc]initWithNibName:@"YJLWriteCodeViewController" bundle:nil];
    
    
    [self.navigationController pushViewController:writeCodeVC animated:YES];
    /**
     *  back 字多了会设置失败的。
     */
    
//    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"填写短信校验码" style:UIBarButtonItemStylePlain target:self action:nil];
//    UIBarButtonItem * leftBtn = [[UIBarButtonItem alloc]init];
//    
//    leftBtn.title = @"填写校验码";
//    
//    self.navigationItem.backBarButtonItem = leftBtn;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
