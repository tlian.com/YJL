//
//  YJLLoginViewController.h
//  YJL
//
//  Created by gtkg－mac on 15-4-21.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YJLLoginViewController : UIViewController

/**
 *  登录按钮
 */
@property (weak, nonatomic) IBOutlet UIButton *logButton;

/**
 *  用户名
 */
@property (weak, nonatomic) IBOutlet UITextField *userName;

/**
 *  密码
 */
@property (weak, nonatomic) IBOutlet UITextField *password;


@end
