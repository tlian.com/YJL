//
//  YJLLoginViewController.m
//  YJL
//
//  Created by gtkg－mac on 15-4-21.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import "YJLLoginViewController.h"
#import "UIBarButtonItem+YJL.h"
#import "YJLFindPasswordViewController.h"
#import "YJLRegisterViewController.h"

@interface YJLLoginViewController ()

@end

@implementation YJLLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //设置登录按钮的圆角 812628376
    self.logButton.layer.masksToBounds = YES;
    self.logButton.layer.cornerRadius = 4;
    
    [self setupNavBar];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 *  设置导航栏内容
 */
-(void)setupNavBar{
    
    // 设置中间icon
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, 0, 100, 40)];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"会  员  登  录";
    titleLabel.font = [UIFont fontWithName:@"Microsoft YaHei" size:22];
    self.navigationItem.titleView = titleLabel;
}


/**
 *  登录事件
 *
 *  @param sender
 */
- (IBAction)loginAction:(id)sender {

}

/**
 *  注册事件
 *
 *  @param sender
 */
- (IBAction)reginAction:(id)sender {
    YJLRegisterViewController *registerVC = [[YJLRegisterViewController alloc]initWithNibName:@"YJLRegisterViewController" bundle:nil];
    [self.navigationController pushViewController:registerVC animated:YES];
}

/**
 *  找回密码事件
 *
 *  @param sender
 */
- (IBAction)lookForPasswordAction:(id)sender {
    
    YJLFindPasswordViewController *finePassVC = [[YJLFindPasswordViewController alloc]initWithNibName:@"YJLFindPasswordViewController" bundle:nil];
    
    /**
     *  back
     */
    UIBarButtonItem * leftBtn = [[UIBarButtonItem alloc]init];
    leftBtn.title = @"找回密码";
    
    self.navigationItem.backBarButtonItem = leftBtn;
    
    [self.navigationController pushViewController:finePassVC animated:YES];
}

-(void)backVC{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
