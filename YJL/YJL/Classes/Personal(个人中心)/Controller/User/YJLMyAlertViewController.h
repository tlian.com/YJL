//
//  YJLMyAlertViewController.h
//  YJL
//
//  Created by 程松斌 on 15/4/26.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YJLMyAlertViewController : UIViewController

//头标题
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

//确认按钮
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;

//取消按钮
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;



@end
