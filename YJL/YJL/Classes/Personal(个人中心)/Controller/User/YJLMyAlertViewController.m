//
//  YJLMyAlertViewController.m
//  YJL
//
//  Created by 程松斌 on 15/4/26.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import "YJLMyAlertViewController.h"

@interface YJLMyAlertViewController ()

@end

@implementation YJLMyAlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.confirmButton.layer.masksToBounds = YES;
    self.confirmButton.layer.cornerRadius = 4;
    self.cancelButton.layer.masksToBounds = YES;
    self.cancelButton.layer.cornerRadius = 4;
    
    self.view.layer.masksToBounds = YES;
    self.view.layer.cornerRadius = 4;
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)didConfirm:(id)sender {
    [[NSNotificationCenter defaultCenter]postNotificationName:@"myAlter" object:@"YES"];
    
}

- (IBAction)didCancel:(id)sender {
    [[NSNotificationCenter defaultCenter]postNotificationName:@"myAlter" object:@"NO"];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
