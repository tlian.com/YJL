//
//  YJLPersonalViewController.h
//  YJL
//
//  Created by tlian on 15/4/20.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YJLMyAlertViewController.h"


@interface YJLPersonalViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

//个人中心的collectView
@property (strong, nonatomic)  UICollectionView *userColletionView;

//个人中心的tableView
@property (weak, nonatomic) IBOutlet UITableView *userTableView;

//collect的数据
@property (strong, nonatomic) NSArray *userCollectArray;

//table的数据
@property (strong, nonatomic) NSArray *userTableArray;

//alertView弹出实图
@property (strong, nonatomic) YJLMyAlertViewController *myAlertView;

@end
