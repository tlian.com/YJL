//
//  YJLPersonalViewController.m
//  YJL
//
//  Created by tlian on 15/4/20.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import "YJLPersonalViewController.h"
#import "UIBarButtonItem+YJL.h"
#import "YJLLoginViewController.h"
#import "YJLOrderViewController.h"

@interface YJLPersonalViewController ()

@end

@implementation YJLPersonalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //注册点击alertView的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAlertView:) name:@"myAlter"object:nil];
    
    //设置导航栏内容
    [self setupNavBar];
    
    //初始化数组
    [self initUserArray];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
//初始化数组
-(void)initUserArray{
    
    self.userTableArray = [[NSArray alloc]initWithObjects:@"我的收藏",@"我的足迹",@"意见反馈", nil];
    
    self.userCollectArray = [[NSArray alloc]initWithObjects:@"我的订单",@"待付款",@"待收货",@"优惠卷",@"资  金",@"客  服", nil];
    
   
    self.userTableView.delegate = self;
    self.userTableView.dataSource = self;
    //这2句话完全没作用？为什么我的电脑要加这句话。
    self.userTableView.tableHeaderView = [[UIView alloc]init];
    self.userTableView.tableFooterView = [[UIView alloc]init];
    UICollectionViewFlowLayout *layout= [[UICollectionViewFlowLayout alloc]init];
    self.userColletionView = [[UICollectionView alloc]initWithFrame:CGRectMake(40, 180, 239, 143) collectionViewLayout:layout];
    [self.userColletionView registerClass:[UICollectionViewCell class]
            forCellWithReuseIdentifier:@"userCollectionCell"];
    self.userColletionView.delegate = self;
    self.userColletionView.dataSource = self;
    self.userColletionView.backgroundColor = [UIColor whiteColor];
    
    self.userColletionView.scrollEnabled = NO;
    self.userTableView.scrollEnabled = NO;
    //设置界面样式
    [self setUpView];
    
    [self.view addSubview:self.userColletionView];
}

/**
 *  设置导航栏内容
 */
-(void)setupNavBar{
    
    // 左边文字
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"个人中心" style:UIBarButtonItemStylePlain target:self action:nil];
    
    // 右边按钮
     self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithIcon:@"set_up" highIcon:@"set_up" target:self action:@selector(userSetUpAction:)];
    
    // 设置中间icon
    UIImageView *userIconImage = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2, 0, 100, 30)];
    userIconImage.image = [UIImage imageNamed:@"logo"];
    self.navigationItem.titleView = userIconImage;
}

//设置界面样式
-(void)setUpView{
    self.userColletionView.layer.masksToBounds = YES;
    self.userColletionView.layer.cornerRadius = 4;
    self.userColletionView.layer.borderWidth = 1;
    self.userColletionView.layer.borderColor = [UIColor grayColor].CGColor;
}
//个人中心的设置点击事件
-(void)userSetUpAction:(id)sender{
    self.myAlertView = [[YJLMyAlertViewController alloc]initWithNibName:@"YJLMyAlertViewController" bundle:nil];
    self.myAlertView.view.frame = CGRectMake(30, 100, 225, 110);
    [self.view addSubview:self.myAlertView.view];
    
}

//接受到通知的方法
-(void)didAlertView:(NSNotification *)notification{
   NSString *alterStr = [notification object];//获取到传递的对象
    if ([alterStr isEqualToString:@"YES"]) {
        YJLLoginViewController *logVC = [[YJLLoginViewController alloc]initWithNibName:@"YJLLoginViewController" bundle:nil];
        /**
         *  back
         */
        UIBarButtonItem * leftBtn = [[UIBarButtonItem alloc]init];
        leftBtn.title = @"";
        
        self.navigationItem.backBarButtonItem = leftBtn;
        
        [self.navigationController pushViewController:logVC animated:YES];
        
    }else{
        //no
    }
    [self.myAlertView.view removeFromSuperview];

}
#pragma mark- TableView datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.userTableArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdenetify = @"theCell";
    UITableViewCell *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:cellIdenetify];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdenetify];
        
    }
    cell.imageView.image = [UIImage imageNamed:self.userTableArray[indexPath.row]];
    cell.textLabel.text = self.userTableArray[indexPath.row];
    cell.textLabel.font = [UIFont fontWithName:@"Microsoft YaHei" size:15];
    UIImageView *imageIndeator = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-50, 5, 30, 30)];
    imageIndeator.image = [UIImage imageNamed:@"right"];
    [cell.contentView addSubview:imageIndeator];
    return cell;
}

#pragma mark- collectView DataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.userCollectArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * CellIdentifier = @"userCollectionCell";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    UIImageView *headImage = [[UIImageView alloc]initWithFrame:CGRectMake(20, 5, 35, 35)];
    headImage.image = [UIImage imageNamed:self.userCollectArray[indexPath.row]];
    UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 40, 40, 20)];
    nameLabel.font = [UIFont fontWithName:@"Microsoft YaHei" size:10];
    nameLabel.textAlignment = NSTextAlignmentCenter;
    nameLabel.text = self.userCollectArray[indexPath.row];
//    cell.backgroundColor = [UIColor redColor];
    [cell.contentView addSubview:headImage];
    [cell.contentView addSubview:nameLabel];
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.userColletionView.frame.size.width/3-10,self.userColletionView.frame.size.width/3-10);
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row >= 0 && indexPath.row < 3) {
        YJLOrderViewController *orderVC = [[YJLOrderViewController alloc]initWithNibName:@"YJLOrderViewController" bundle:nil];
        
        /**
         *  back
         */
        UIBarButtonItem * leftBtn = [[UIBarButtonItem alloc]init];
        leftBtn.title = @"我的订单";
        
        self.navigationItem.backBarButtonItem = leftBtn;
        
        [self.navigationController pushViewController:orderVC animated:YES];
        
    }
}
@end
