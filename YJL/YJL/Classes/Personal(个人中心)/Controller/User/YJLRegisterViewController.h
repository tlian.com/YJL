//
//  YJLRegisterViewController.h
//  YJL
//
//  Created by gtkg－mac on 15-4-22.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YJLRegisterViewController : UIViewController
//用户名
@property (weak, nonatomic) IBOutlet UITextField *userName;

//密码
@property (weak, nonatomic) IBOutlet UITextField *password;

//二次输入的密码
@property (weak, nonatomic) IBOutlet UITextField *passwordAgain;

//邮箱
@property (weak, nonatomic) IBOutlet UITextField *email;

//手机号码
@property (weak, nonatomic) IBOutlet UITextField *phone;

//注册按钮
@property (weak, nonatomic) IBOutlet UIButton *registButton;














@end
