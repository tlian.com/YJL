//
//  YJLRegisterViewController.m
//  YJL
//
//  Created by gtkg－mac on 15-4-22.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import "YJLRegisterViewController.h"
#import "UIBarButtonItem+YJL.h"

@interface YJLRegisterViewController ()

@end

@implementation YJLRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.registButton.layer.masksToBounds = YES;
    self.registButton.layer.cornerRadius = 4;
    
    // 左边文字
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithIcon:@"icon_return" highIcon:@"icon_return" target:self action:@selector(backVC)];
    
    // 设置中间icon
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, 0, 100, 40)];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"个  人  注  册";
    titleLabel.font = [UIFont fontWithName:@"Microsoft YaHei" size:22];
    self.navigationItem.titleView = titleLabel;
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)backVC{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
