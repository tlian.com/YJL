//
//  YJLResetPasswordViewController.h
//  YJL
//
//  Created by 程松斌 on 15/4/26.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YJLResetPasswordViewController : UIViewController
//新密码
@property (weak, nonatomic) IBOutlet UITextField *myPassword;

//下一步
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@end
