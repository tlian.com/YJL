//
//  YJLResetPasswordViewController.m
//  YJL
//
//  Created by 程松斌 on 15/4/26.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import "YJLResetPasswordViewController.h"
#import "YJLLoginViewController.h"

@interface YJLResetPasswordViewController ()

@end

@implementation YJLResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //由于字太多，只能这么写了。
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"<重置登录密码" style:UIBarButtonItemStyleBordered target:self action:@selector(backVC)];
    self.navigationItem.leftBarButtonItem= backButton;
    
    self.nextButton.layer.masksToBounds = YES;
    self.nextButton.layer.cornerRadius = 4;
    self.nextButton.layer.borderWidth = 1;
    self.nextButton.layer.borderColor = [UIColor grayColor].CGColor;
    self.myPassword.layer.masksToBounds = YES;
    self.myPassword.layer.cornerRadius = 4;
    self.myPassword.layer.borderWidth = 1;
    self.myPassword.layer.borderColor = [UIColor grayColor].CGColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/**
 *  下一步，返回根目录，然后在根目录判断是否登录
 *
 *  @param sender
 */
- (IBAction)nextAction:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
/**
 *  返回上个界面
 */
-(void)backVC{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
