//
//  YJLWriteCodeViewController.h
//  YJL
//
//  Created by 程松斌 on 15/4/26.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YJLWriteCodeViewController : UIViewController


//显示获取手机号码的提示
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

//短信校验码
@property (weak, nonatomic) IBOutlet UITextField *code;

//点击获取校验码的按钮
@property (weak, nonatomic) IBOutlet UIButton *getCodeButton;

//下一步按钮
@property (weak, nonatomic) IBOutlet UIButton *nextButton;



@end
