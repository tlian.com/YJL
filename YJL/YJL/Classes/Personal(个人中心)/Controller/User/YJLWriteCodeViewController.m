//
//  YJLWriteCodeViewController.m
//  YJL
//
//  Created by 程松斌 on 15/4/26.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import "YJLWriteCodeViewController.h"
#import "YJLResetPasswordViewController.h"

@interface YJLWriteCodeViewController ()

@end

@implementation YJLWriteCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //由于字太多，只能这么写了。
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"<填写短信校验码" style:UIBarButtonItemStyleBordered target:self action:@selector(backVC)];
    self.navigationItem.leftBarButtonItem= backButton;
    
    self.getCodeButton.layer.masksToBounds = YES;
    self.getCodeButton.layer.cornerRadius = 4;
    self.getCodeButton.layer.borderWidth = 1;
    self.getCodeButton.layer.borderColor = [UIColor grayColor].CGColor;
    
    self.code.layer.masksToBounds = YES;
    self.code.layer.cornerRadius = 4;
    self.code.layer.borderWidth = 1;
    self.code.layer.borderColor = [UIColor grayColor].CGColor;
    
    self.nextButton.layer.masksToBounds = YES;
    self.nextButton.layer.cornerRadius = 4;
    self.nextButton.layer.borderWidth = 1;
    self.nextButton.layer.borderColor = [UIColor grayColor].CGColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//下一步
- (IBAction)next:(id)sender {
    YJLResetPasswordViewController *resetPassVC = [[YJLResetPasswordViewController alloc]initWithNibName:@"YJLResetPasswordViewController" bundle:nil];
    /**
     *  back
     */
//    UIBarButtonItem * leftBtn = [[UIBarButtonItem alloc]init];
//    leftBtn.title = @"重置密码";
//    
//    self.navigationItem.backBarButtonItem = leftBtn;
    
    [self.navigationController pushViewController:resetPassVC animated:YES];
}

-(void)backVC{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
