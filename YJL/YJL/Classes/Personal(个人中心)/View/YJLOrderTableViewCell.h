//
//  YJLOrderTableViewCell.h
//  YJL
//
//  Created by gtkg－mac on 15-4-27.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YJLOrderTableViewCell : UITableViewCell
/**
 *  订单图片
 */
@property (weak, nonatomic) IBOutlet UIImageView *orderImage;
/**
 *  订单名字
 */
@property (weak, nonatomic) IBOutlet UILabel *orderName;

/**
 *  订单价格
 */
@property (weak, nonatomic) IBOutlet UILabel *orderPrice;

/**
 *  订单打折前的价格
 */
@property (weak, nonatomic) IBOutlet UILabel *orderDicountPri;

/**
 *  订单商品的数量
 */
@property (weak, nonatomic) IBOutlet UILabel *orderCount;

/**
 *  订单商品的总数
 */
@property (weak, nonatomic) IBOutlet UILabel *orderCountState;

/**
 *  订单实际支付的价格
 */
@property (weak, nonatomic) IBOutlet UILabel *orderPay;

/**
 *  订单状态
 */
@property (weak, nonatomic) IBOutlet UILabel *orderState;

/**
 *  订单的动作
 */
@property (weak, nonatomic) IBOutlet UIButton *orderAction;

@end
