//
//  YJLOrderTableViewCell.m
//  YJL
//
//  Created by gtkg－mac on 15-4-27.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import "YJLOrderTableViewCell.h"

@implementation YJLOrderTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.orderAction.layer.masksToBounds = YES;
    self.orderAction.layer.cornerRadius  = 4;
    self.orderAction.layer.borderWidth = 1;
    self.orderAction.layer.borderColor = [UIColor grayColor].CGColor;
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
