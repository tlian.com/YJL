//
//  YJLShoppingCartViewController.h
//  YJL
//
//  Created by tlian on 15/4/20.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YJLShoppingCartViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

//购物车里面东西的个数
@property (strong, nonatomic) NSMutableArray *cartArray;

//没有商品的时候显示的view
@property (weak, nonatomic) IBOutlet UIView *noGoodsView;
//购物按钮
@property (weak, nonatomic) IBOutlet UIButton *shoppingButton;

// 购物车的列表
@property (strong, nonatomic) UITableView *cartTableView;
/**
 *  结算按钮
 */
@property (weak, nonatomic) IBOutlet UIButton *settleAccount;
/**
 *  结算界面
 */
@property (weak, nonatomic) IBOutlet UIView *settleAccountView;


@end
