//
//  YJLShoppingCartViewController.m
//  YJL
//
//  Created by tlian on 15/4/20.
//  Copyright (c) 2015年 tlian. All rights reserved.
//

#import "YJLShoppingCartViewController.h"
#import "UIBarButtonItem+YJL.h"
#import "YJLCartTableViewCell.h"

@interface YJLShoppingCartViewController ()

@end

@implementation YJLShoppingCartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //设置导航栏的文字
    [self setupNavBar];
    
    self.cartArray = [NSMutableArray array];
   
    //设置view的样式
    [self setupView];
    
    //添加tableView
    [self createTableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/**
 *  设置导航栏内容
 */
-(void)setupNavBar{
    
    // 左边文字
    NSString *cartStr = [NSString stringWithFormat:@"购物车（%ld）",(unsigned long)self.cartArray.count];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:cartStr style:UIBarButtonItemStylePlain target:self action:nil];
    //中间的设置
    UIView *nullView = [[UIView alloc]init];
    self.navigationItem.titleView = nullView;
    // 右边按钮
//    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithIcon:@"set_up" highIcon:@"set_up" target:self action:@selector(userSetUpAction:)];
  
}

/**
 *  设置界面的样式
 */
-(void)setupView{
    self.shoppingButton.layer.masksToBounds = YES;
    self.shoppingButton.layer.cornerRadius = 4;
    self.settleAccount.layer.masksToBounds = YES;
    self.settleAccount.layer.cornerRadius = 4;
}

/**
 *  创建tableView
 */
-(void)createTableView{
    self.cartTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT-214)];
    self.cartTableView.delegate = self;
    self.cartTableView.dataSource = self;
    self.cartTableView.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:self.cartTableView];
}

#pragma mark- TableView datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //    return self.orderArray.count;
    return 4;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdenetify = @"cartCell";
    BOOL nib = NO;
    if (!nib) {
        [tableView registerNib:[UINib nibWithNibName:@"YJLCartTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdenetify];
        nib = YES;
    }
    YJLCartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdenetify];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}



@end
